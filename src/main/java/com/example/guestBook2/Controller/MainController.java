package com.example.guestBook2.Controller;

import com.example.guestBook2.DAO.ArticleRepo;
import com.example.guestBook2.DAO.CommentRepo;
import com.example.guestBook2.Model.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class MainController {

    @Autowired
    ArticleRepo articleRepo;

    @Autowired
    CommentRepo commentRepo;

    //@GetMapping
    @RequestMapping("/main")
    public String allArticle(Model model) {
        model.addAttribute("articles", articleRepo.findAll());
        return "main";
    }

    @RequestMapping("/addArticle")
    public String addArticle(Model model) {
        return "addArticle";
    }

    @RequestMapping("/save")
    public String save(@RequestParam String title, @RequestParam String author,
                       @RequestParam String text) {
        Article article = new Article();
        article.setTitle(title);
        article.setAuthor(author);
        article.setText(text);
        articleRepo.save(article);

        return "redirect:/article/" + article.get_id();
    }

    @RequestMapping("/article/{id}")
    public String show(@PathVariable String id, Model model) {
        model.addAttribute("articles", articleRepo.getBy_id(id));
        return "article";
    }

    @RequestMapping("/delete")
    public String delete(@RequestParam String id) {
        Article article = articleRepo.getBy_id(id);
        articleRepo.delete(article);

        return "redirect:/main";
    }

    @RequestMapping("/editArticle/{id}")
    public String edit(@PathVariable String id, Model model) {
        model.addAttribute("article", articleRepo.getBy_id(id));
        return "editArticle";
    }

    @RequestMapping("/update")
    public String update(@RequestParam String id, @RequestParam String title, @RequestParam String text,
                         @RequestParam String author) {
        Article article = articleRepo.getBy_id(id);
        article.setTitle(title);
        article.setAuthor(author);
        article.setText(text);
        articleRepo.save(article);

        return "redirect:/article/" + article.get_id();
    }


}
