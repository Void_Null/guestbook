package com.example.guestBook2.DAO;


import com.example.guestBook2.Model.Comment;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CommentRepo extends CrudRepository<Comment, String> {
    List<Comment> findByParentId(String parentId);
    void deleteAllByParentId(String parentId);
}
