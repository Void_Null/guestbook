package com.example.guestBook2.DAO;

import com.example.guestBook2.Model.Article;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ArticleRepo extends MongoRepository<Article, String> {
    List<Article> findAll();
    List<Article> findByTitle(String title);

    Article getBy_id(String _id);
}
